#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
LANG=en_US.UTF-8

startTime=`date +%s`

if [ $(whoami) != "root" ];then
	echo "请使用root权限执行命令！"
	exit 1;
fi

is64bit=$(getconf LONG_BIT)
if [ "${is64bit}" != '64' ];then
	Red_Error "抱歉, 当前版本不支持32位系统, 请使用64位系统!";
fi

Centos6Check=$(cat /etc/redhat-release | grep ' 6.' | grep -iE 'centos|Red Hat')
if [ "${Centos6Check}" ];then
	echo "Centos6不支持安装nnws，请更换Centos7/8安装nnws"
	exit 1
fi 

UbuntuCheck=$(cat /etc/issue|grep Ubuntu|awk '{print $2}'|cut -f 1 -d '.')
if [ "${UbuntuCheck}" ] && [ "${UbuntuCheck}" -lt "16" ];then
	echo "Ubuntu ${UbuntuCheck}不支持安装nnws，建议更换Ubuntu18/20安装nnws"
	exit 1
fi

cd /
if [ -s "/nnws" ];then
	cd nnws
elif [ -s "/etc/issue" ]; then
	mkdir nnws
    cd nnws
fi

if [ -s "/nnws/nnws" ];then
	rm -f /nnws/nnws
fi

wget https://gitee.com/chen-binfa/nnws/releases/download/v1.1.0/nnws

chmod +x nnws
nohup ./nnws &

Get_Ip_Address(){
	getIpAddress=""
	getIpAddress=$(curl -sS --connect-timeout 10 -m 60 https://www.bt.cn/Api/getIpAddress)
}

Get_Ip_Address

echo -e "=================================================================="
echo -e "\033[32mCongratulations! Installed successfully!\033[0m"
echo -e "===========================nnws安装成功============================"
echo -e ""
echo -e " 网站管理后台地址: http://${getIpAddress}:3009/login.html"
echo -e " username: admin"
echo -e " password: admin888"
echo -e " "
echo -e "=============================注意事项=============================="
echo -e ""
echo -e " 【登录后请马上修改默认登录账号和密码】"
echo -e " 【请在安全组放行 3009 端口】"
echo -e " 【查看日志方式：tail -F /nnws/nohup.out】"
echo -e "" 
echo -e "=================================================================="
endTime=`date +%s`
((outTime=($endTime-$startTime)/60))
echo -e "Time consumed:\033[32m $outTime \033[0mMinute!"
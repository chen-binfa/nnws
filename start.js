#!/usr/bin/env node

const cluster = require("cluster");
const appmain = require("./src/app.js");
const os = require("os");
const platform = os.platform();

console.log("操作系统", platform);
console.log("process.cwd:", process.cwd());
console.log("__dirname:", __dirname);

if (cluster.isMaster) {
  console.log(`主进程 ${process.pid} 正在运行`);
  // Fork workers.
  cluster.fork();
  cluster.on("exit", (worker, code, signal) => {
    console.log(`子进程 ${worker.process.pid} 已经退出`);
  });
  cluster.on("message", (child, message) => {
    // console.log("主进程收到消息：", message);
    if (message == "重启") {
      setTimeout(function () {
        child.kill();
        cluster.fork();
      }, 200);
    }
  });
} else {
  console.log(`子进程 ${process.pid} 已经启动`);
  appmain();
}
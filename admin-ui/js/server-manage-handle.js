const serverHander = {
  save: (list) => {
    store.set("servers", list);
  },
  getList: () => {
    let servers = store.get("servers");
    if (servers) {
      return servers;
    }
    let h=window.location.href;
    if(h.includes('gitee.io')){
      return [];
    }
    let u = new URL(h);
    let entity = {
      api: u.origin,
      label: u.hostname,
      curr: true,
      status: true,
    };
    servers = [entity];
    serverHander.save(servers);    
    return servers;
  },
  getCurrAPI: () => {
    let list = serverHander.getList();
    let o=list.find((t) => t.curr);
    if(o&&o.api){
        return o.api;
    }
    return '';
  },
  setCurr: (api) => {
    let list = serverHander.getList();
    list.forEach((t) => (t.curr = t.api == api));
    serverHander.save(list);
  },
  del: (api) => {
    let list = serverHander.getList();
    let i = list.findIndex((t) => t.api == api);
    if (i > -1) {
      list.splice(i, 1);
    }
    serverHander.save(list);
  },
  updateStatus:(api,status)=>{
    let list = serverHander.getList();
    let i = list.findIndex((t) => t.api == api);
    if (i > -1) {
      list[i].status=status;
    }
    serverHander.save(list);
  },
  add: (label, api) => {
    if (api.slice(-1) == "/") {
      api = api.slice(0, api.length - 1);
    }
    if (!label) {
      let u = new URL(api);
      label = u.hostname;
    }
    let list = serverHander.getList();
    let i = list.findIndex((t) => t.api == api);
    if (i == -1) {
      list.push({
        label,
        api,
        status: false,
        curr: false,
      });
    } else {
      list[i].label = label;
      list[i].api = api;
    }
    serverHander.save(list);
  },
};

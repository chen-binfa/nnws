var vm = new Vue({
  el: "#app",
  data: {
    loading: false,
    action: "list",
    newServer: {
      label: "",
      api: "",
    },
    servers: [],
    currServerAPI: "",
  },
  mounted() {
    this.loadServers();
  },
  methods: {
    loadServers() {
      this.servers = serverHander.getList();
      this.currServerAPI = serverHander.getCurrAPI();
    },
    onAddAPI() {
      let that = this;
      let n = this.newServer;
      if (!n.api) {
        that.$message({
          message: "请输入API地址",
          type: "error",
        });
        return;
      }
      serverHander.add(n.label, n.api);
      that.$message({
        message: "添加成功！",
        type: "success",
      });
      this.loadServers();
      this.action = "list";
    },
    onChangeServer(api) {
      this.currServerAPI = api;
      serverHander.setCurr(api);
      this.loadServers();
    },
    onDelServer(api) {
      serverHander.del(api);
      this.loadServers();
    },
    testStatus(api) {
      let that = this;
      this.loading=true;
      ajax.post(api + "/api/testStatus", {}).then(
        (t) => {    
          let status=false;      
          if (t.msg == "ok") {            
            that.$message({
              message: "连接成功！",
              type: "success",
            });
            status=true;      
          } else {
            serverHander.updateStatus(api, false);
            that.loadServers();
            that.$message({
              message: "连接失败:" + t.msg,
              type: "error",
            });
          }
          that.loading=false;
          serverHander.updateStatus(api, status);
          that.loadServers();
        },
        (e) => {
          serverHander.updateStatus(api, false);
          that.loadServers();
          that.loading=false;
          that.$message({
            message: "连接失败:" + e.message,
            type: "error",
          });
        }
      );
    },
  },
});

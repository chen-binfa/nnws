function timestr(fmt, time) {
  var now = time ? new Date(time) : new Date();
  var o = {
    "M+": now.getMonth() + 1, //月份
    "d+": now.getDate(), //日
    "h+": now.getHours(), //小时
    "m+": now.getMinutes(), //分
    "s+": now.getSeconds(), //秒
    "q+": Math.floor((now.getMonth() + 3) / 3), //季度
    S: now.getMilliseconds(), //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(
      RegExp.$1,
      (now.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
      );
  return fmt;
}
const ajax = {
  post: function (url, params = {}) {
    return new Promise((resolve, reject) => {
      let token = localStorage.getItem("token");
      // params.token=token;
      if (url.indexOf("http") == -1) {
        url = serverHander.getCurrAPI() + url;
      }
      fetch(url, {
        method: "POST",
        headers: {
          Accept: "*/*",
          "Content-Type": "application/json;charset=utf-8",
          token: token,
        },
        body: JSON.stringify(params),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          if (data.msg == "unlogin") {
            return (window.location.href = "/login.html?unlogin");
          }
          resolve(data);
        })
        .catch((error) => {
          console.log(error.msg || error);
          resolve({
            msg: error.message,
          });
        });
    });
  },
};

const store = {
  get: (key) => {
    let str = localStorage.getItem(key);
    let json = null;
    if (!str) {
      return str;
    }
    try {
      json = JSON.parse(str);
    } catch (e) {
      console.log(e);
    }
    if (!json || typeof json != "object") {
      return str;
    }
    if (!json.time || !json.data) {
      return json;
    }
    return json.data;
  },
  set: (key, value) => {
    let time = new Date().valueOf();
    let json = {
      time,
      data: value,
    };
    localStorage.setItem(key, JSON.stringify(json));
  },
  remove: (key) => {
    localStorage.removeItem(key);
  },
  clear: () => {
    localStorage.clear();
  },
};

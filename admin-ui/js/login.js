var vm = new Vue({
  el: "#app",
  data: {
    loading: false,
    ruleForm: {
      username: "",
      password: "",
    },
    rules: {
      username: [{ required: true, message: "请输入用户名", trigger: "blur" }],
      password: [{ required: true, message: "请输入密码", trigger: "blur" }],
    },
    dialogVisibleList: false,
    servers: [],
    currServerAPI:''
  },
  mounted() {
    const that = this;
    document.onkeydown = function (event) {
      var e = event || window.event || arguments.callee.caller.arguments[0];
      if (e && e.keyCode == 13) {
        // enter 键
        //要做的事情
        that.submitForm("ruleForm");
      }
    };
    this.loadServers();
  },
  methods: {
    loadServers() {
      this.servers = serverHander.getList();
      this.currServerAPI=serverHander.getCurrAPI();
    },
    submitForm(formName) {
      const that = this;
      if(!this.currServerAPI){
        return that.$message({
          message: "请先添加服务器",
          type: "error",
        });
      }
      this.$refs[formName].validate((valid) => {
        if (valid) {
          ajax.post("/api/login", that.ruleForm).then((t) => {
            console.log("data:", t);
            if (t.msg == "ok" && t.data) {
              that.$message({
                message: "登录成功！",
                type: "success",
              });
              localStorage.setItem("token", t.data);
              window.location.href = "manage";
            } else {
              that.$message({
                message: "登录失败:" + t.msg,
                type: "error",
              });
            }
          }, console.log);
        } else {
          console.log("error submit!!");
          return false;
        }
      });
    },    
    onChangeServer(){
      serverHander.setCurr(this.currServerAPI);
    },
    ifrBack(){
      this.dialogVisibleList=false;
      this.loadServers();
    }
  },
});

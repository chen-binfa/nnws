var vm = new Vue({
  el: "#app",
  data: {
    loading: false,
    dialogVisible: false,
    dialogVisibleAdmin: false,
    dialogVisibleRestart: false,
    srcList: [], //原列表
    showList: [],
    newItem: {},
    currNav: "list",
    multipleSelection: [],
    admin: {
      username: "",
      password: "",
      update: 1,
    },
    currNav: "",
    showNote: false,
    showIfr: false
  },
  beforeMount: function () {
    this.initNewItem();
    this.loadList();
  },
  mounted() { },
  methods: {
    tableRowClassName({ row, rowIndex }) {
      return row.enable ? "success-row" : "warning-row";
    },
    ifrBack() {
      this.showIfr = false;
      window.location.reload();
    },
    initNewItem() {
      this.newItem = {
        enable: true,
        showLog: false,
        name: "",
        host: "localhost",
        staticDirs: [],
        history: true,
        cors: false,
        http: {
          enable: true,
          port: 80,
        },
        https: {
          enable: false,
          port: 443,
        },
        proxys: []
      };
    },
    onAddProxyConfig(newItem) {
      newItem.proxys.push({
        enable: true,
        showPostData: true,
        target: "http://localhost:443",
        path: "/api/*",
        rewrite: ""
      });
    },
    onRemoveProxyConfig(newItem, i) {
      newItem.proxys.splice(i, 1);
    },
    loadList() {
      const that = this;
      let params = {
        way: "website",
        action: "list",
      };
      this.currNav = "list";
      this.loading = true;
      ajax.post("/api/manage", params).then((t) => {
        that.loading = false;
        if (t.msg == "ok") {
          if (!t.data) t.data = [];
          that.srcList = t.data;
          that.showList = that.formatList(t.data);
        } else {
          that.$alert("操作失败:" + t.msg);
        }
      }, console.log);
    },
    submitForm(formName) {
      const that = this;
      console.log(formName);

      if (!that.newItem.name) {
        return that.$message({
          message: "请输入网站名称",
          type: "error",
        });
      }
      if (!that.newItem.host) {
        return that.$message({
          message: "请输入域名",
          type: "error",
        });
      }
      if (
        that.newItem.http?.port == "888" ||
        that.newItem.https?.port == "888"
      ) {
        return that.$message({
          message: "不能使用888端口",
          type: "error",
        });
      }
      let params = this.clone(that.newItem);
      console.log("params.staticDirs", params.staticDirs);
      let staticDirs = params.staticDirs;
      params.staticDirs = staticDirs && (typeof staticDirs == 'string') ? staticDirs.split(",") : [];
      params.enable = params.http.enable || params.https.enable;
      params.way = "website";
      params.action = "save";
      ajax.post("/api/manage", params).then((t) => {
        console.log("data:", t);
        if (t.msg == "ok") {
          that.initNewItem();
          that.dialogVisible = false;
          that.$message({
            message: "保存成功",
            type: "success",
          });
          that.loadList();
          that.showNote = true;
        } else {
          that.$alert("操作失败:" + t.msg);
        }
      }, console.log);
    },
    formatList(l) {
      let list = this.clone(l);
      list.forEach((t) => {
        // t.status = t.enable ? "▶️ 运行中" : "⛔ 已停止";
        // t.http = t.http && t.http.enable ? t.http.port : "未开启";
        // t.https = t.https && t.https.enable ? t.https.port : "未开启";
        // let proxys =
        //   t.proxys && Array.isArray(t.proxys)
        //     ? t.proxys.filter((f) => f.enable)
        //     : [];
        // t.proxys =
        //   proxys.length > 0
        //     ? proxys.map((m) => m.path + "⇒" + m.target).join(" ; ")
        //     : "无";
        t.showLog = t.showLog ? "✓" : "";
        t.history = t.history ? "✓" : "";
        t.cors = t.cors ? "✓" : "";
        // t.staticDirs = t.staticDirs;
      });
      return list;
    },
    onSelectionChange(val) {
      this.multipleSelection = val;
      console.log(val);
    },
    onCreateStart() {
      this.currNav = "create";
      this.dialogVisible = true;
      this.initNewItem();
    },
    onEditStart(rowId) {
      let id = rowId || this.multipleSelection[0].id;
      let entity = this.srcList.find((t) => t.id == id);
      entity = this.clone(entity);
      entity.staticDirs = entity.staticDirs ? entity.staticDirs.join(",") : "";
      this.newItem = entity;
      this.dialogVisible = true;
    },
    onDel(idArr) {
      const that = this;
      let ids = idArr || this.multipleSelection.map((t) => t.id);
      that.$confirm("确定要删除[" + ids.length + "]条记录吗？", {
        callback: function (a) {
          if (a == "cancel") {
            return;
          }
          const params = {
            way: "website",
            action: "del",
            ids: ids,
          };
          ajax.post("/api/manage", params).then((t) => {
            console.log("data:", t);
            if (t.msg == "ok") {
              that.loadList();
              that.showNote = true;
            } else {
              that.$alert("操作失败:" + t.msg);
            }
          }, console.log);
        },
      });
    },
    clone(o) {
      return JSON.parse(JSON.stringify(o));
    },
    onDialogClose() {
      this.currNav = "list";
    },
    onChangePassword() {
      let that = this;
      let params = this.admin;
      console.log(params);
      ajax.post("/api/login", params).then((t) => {
        console.log("data:", t);
        if (t.msg == "ok") {
          that.$message({
            message: "保存成功,请重新登录",
            type: "success",
          });
          setTimeout(function () {
            window.location.href = "/login.html";
          }, 2000);
        } else {
          that.$alert("操作失败:" + t.msg);
        }
      }, console.log);
    },
    handleRowClick(ac, id) {
      if (ac == "edit") {
        this.onEditStart(id);
      } else {
        this.onDel([id]);
      }
    },
    onRestart() {
      const that = this;
      let params = {
        way: "website",
        action: "restart",
      };
      this.loading = true;
      ajax.post("/api/manage", params).then((t) => {
        that.loading = false;
        that.dialogVisibleRestart = false;
        that.currNav = "list";
        if (t.msg == "ok") {
          that.$alert("重启指令已发送，1秒后自动刷新页面!");
          setTimeout(function () {
            window.location.reload();
          }, 100);
        } else {
          that.$alert("操作失败:" + t.msg);
        }
      }, console.log);
    },
    async checkPortUse(port) {
      if (!port) return;
      const that = this;
      const params = {
        way: "website",
        action: "portIsUsing",
        port,
      };
      const loading = this.$loading({
        lock: true,
        text: 'Loading',
        spinner: 'el-icon-loading',
        background: 'rgba(0, 0, 0, 0.7)'
      });
      let t = await ajax.post("/api/manage", params);
      loading.close();
      console.log("data:", t);
      if (t.msg == "ok") {
        that.$message({
          type: t.data ? 'error' : 'success',
          message: t.data ? "端口已被占用" : "端口未使用"
        });
      } else {
        that.$alert("操作失败:" + t.msg);
      }
    }
  },
});

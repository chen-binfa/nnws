/*
 * @Description:
 * @Autor: fage
 * @Date: 2022-06-06 14:49:49
 * @LastEditors: lanmeng656 cbf0311@sina.com
 * @LastEditTime: 2022-11-14 20:56:08
 */
"use strict";
const db = require("../utils/db");
const jwt = require("../utils/jwt");
let errorTimes = 0;
module.exports = async function (req, res, next) {
  let { username, password, update } = req.body;
  let token = req.headers["token"];
  const admin = db.get("admin");
  if (update && token) {
    let vRet = await jwt.verify(token);
    if (vRet && vRet.data) {
      admin.username = username;
      admin.password = password;
      db.set("admin", admin);
      db.sync();
      res.json({ msg: "ok" });
    } else {
      res.json({ msg: "unlogin" });
    }
  } else {
    if (errorTimes > 4) {
      errorTimes++;
      if (errorTimes == 8) {
        setTimeout((t) => {
          errorTimes = 0;
        }, 3600000 * 5);
        return res.json({ msg: "登录错误次数太多，系统已锁定,5小时后解锁." });
      } else {
        return res.json({ msg: "登录错误次数太多，系统已锁定" });
      }
    }
    if (username == admin.username && admin.password == password) {
      return res.json({ msg: "ok", data: jwt.sign({ username }) });
    }
    errorTimes++;
    res.json({ msg: "用户名或密码错误" });
  }
};

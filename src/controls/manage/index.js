"use strict";
let paramHelper = require("./param-helper");
const jwt = require("../../utils/jwt");

let obj = {
  website: require("../website"),
};

module.exports = async function (req, res, next) {
  try {
    let way = req.body.way;
    let action = req.body.action || "";
    let token = req.headers["token"];
    if (!token) {
      return res.json({ msg: "unlogin", result: "token null" });
    }
    // console.log("headers", req.headers);
    let vRet = await jwt.verify(token);
    if (!vRet) {
      return res.json({
        msg: "unlogin",
        result: "token error,未登录或登录超时" + way + action,
      });
    }
    // if (vRet) {
    //   console.log({ vRet });
    // }
    paramHelper(req);
    let o = obj[way];
    if (o) {
      o(req, res, next);
      return false;
    }
    res.json({
      result: "way未定义:" + way,
      msg: "fail",
    });
  } catch (e) {
    next(e);
  }
};

/*
 * @Description:
 * @Autor: fage
 * @Date: 2022-06-06 15:10:14
 * @LastEditors: lanmeng656 cbf0311@sina.com
 * @LastEditTime: 2022-12-01 16:45:15
 */
"use strict";
const db = require("../utils/db");
const common = require("../utils/common");
const path = require("path");
// let child_process = require("child_process");

module.exports = function (req, res, next) {
  let funs = {
    list: list,
    save: save,
    del: del,
    getDir: getDir,
    restart,
    portIsUsing
  };
  let ret = {
    msg: "ok",
    data: [],
  };
  let f = funs[req.body.action];
  if (f) {
    f(ret, req, res);
  } else {
    ret.msg = "action(" + req.body.action + ")未定义";
    res.json(ret);
  }
};

function restart(ret, req, res) {
  setTimeout(() => {
    try {
      // child_process.exec("pm2 restart express-proxy", console.log);
      process.send("重启");
    } catch (e) {
      console.log(e);
    }
  }, 10);
  res.json({ msg: "ok" });
}
function list(ret, req, res) {
  res.json({ msg: "ok", data: db.get("websites") });
}

function del(ret, req, res) {
  const ids = req.body.ids;
  const list = db.get("websites");
  ids.forEach((id) => {
    const i = list.findIndex((t) => t.id == id);
    list.splice(i, 1);
  });
  db.set("websites", list);
  db.sync();
  res.json({ msg: "ok", data: list });
}
function save(ret, req, res) {
  let id = req.body.id;
  const list = db.get("websites") || [];
  delete req.body.way;
  delete req.body.action;
  if (id) {
    const i = list.findIndex((t) => t.id == id);
    const entity = req.body;
    entity.httpStatus = "pending";
    entity.httpsStatus = "pending";
    list[i] = entity;
  } else {
    req.body.id = new Date().valueOf().toString();
    const entity = req.body;
    entity.httpStatus = "pending";
    entity.httpsStatus = "pending";
    list.push(entity);
  }
  db.set("websites", list);
  db.sync();
  res.json({ msg: "ok", data: list });
}
function getDir(ret, req, res) {
  if (!req.body.dir) {
    req.body.dir = "/";
  }
  let dir = path.join(req.body.dir, "/");
  console.log("req.body.dir", dir);
  res.json({ msg: "ok", data: common.getDir(dir) });
}
async function portIsUsing(ret, req, res) {
  let isUsing = await common.portIsUsing(req.body.port);
  res.json({ msg: "ok", data: isUsing });
}
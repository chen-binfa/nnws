const db = require("../utils/db");
module.exports = {
  getDefaultConfig,
  setDefaultConfig,
};

function getDefaultConfig() {
  return {
    id: 10000,
    enable: true,
    isAdmin: true,
    name: "nnws管理后台",
    host: "localhost",
    staticDirs: ["./admin-ui/"],
    cors: true,
    http: {
      enable: true,
      port: "3009",
    },
    https: {
      enable: false,
      port: 443,
    },
    proxys: [],
    httpStatus: "ok",
    httpsStatus: "pending",
  };
}
function setDefaultConfig() {
  let configs = db.get("websites") || [];
  let config = configs.find((t) => t.isAdmin == true);
  if (!config) {
    config = getDefaultConfig();
    configs.unshift(config);
    db.set("websites", configs);
  }
}

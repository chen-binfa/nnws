const http = require("http");
const https = require("https");
const adminConfig = require("./controls/admin-web-config");
const utils = require("./utils/index");
adminConfig.setDefaultConfig();

function startHttpServer(config) {
  return new Promise((resolve, reject) => {
    // app.listen(config.port);
    let port = config.port || config.http.port;
    const httpServer = http.createServer(config.app);
    doStart(httpServer, config.host, port, "http", resolve);
  });
}
function startHttpsServer(config) {
  return new Promise((resolve, reject) => {
    let sites = [config];
    const port = config.port || config.https.port;
    if (config.sites) {
      // proxy
      sites = config.sites;
    }
    let credentials = utils.getKeyCert(sites);
    if (!credentials) {
      console.log("port", port, "未找到证书，跳过运行");
      return resolve(false);
    }
    const httpsServer = https.createServer(credentials, config.app);
    doStart(httpsServer, config.host, port, "https", resolve);
  });
}
function doStart(server, host, port, type, cb) {
  server.timeout=36000000;
  server.listen(port, function () {
    console.log(type + " listening on", port);
    if (host) {
      let shellCom = type + "://" + host;
      if (port != 443 && port != 80) {
        shellCom += ":" + port;
      }
      console.log("访问地址：", shellCom);
    }
    cb("ok");
  });
  server.on("error", function (err) {
    let errMsg = err.code;
    if (errMsg == "EADDRINUSE") {
      errMsg = "端口被占用";
    } else if (errMsg == "EACCES") {
      errMsg = "没有权限";
    }
    console.log("**错误**：", errMsg);
    cb(errMsg);
  });
}

module.exports = {
  startHttpServer,
  startHttpsServer,
};

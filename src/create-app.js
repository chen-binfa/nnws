//不同端口时
const express = require("express");
const path = require("path");
const fs = require("fs");
const moment = require("moment");
const { getHost } = require("./utils");
const bodyParser = require("body-parser");
const history = require("connect-history-api-fallback");
const cors = require("cors");
const {
  createProxyMiddleware,
  fixRequestBody,
} = require("http-proxy-middleware");
const logger = require("morgan");
const login = require("./controls/login");
const testStatus = require("./controls/testStatus");
const manage = require("./controls/manage");
const compression = require("compression");
const adminConfig = require("./controls/admin-web-config");
adminConfig.setDefaultConfig();

function createExpressApp(config) {
  const app = express();
  app.use(compression()); //gzip
  console.log("CORS:",config.cors);
  if (config.cors) {    
    app.use(cors());
  }
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  if (config.showLog) {
    console.log("显示日志:", true);
    logger.token("time", function (req, res) {
      return moment().format("YYYY-MM-DD HH:mm:ss");
    });
    logger.token("realip", function (req, res) {
      if (!req || !req.ip) return "";
      return req.ip.replace("::ffff:", "");
    });
    logger.token("host", function (req, res) {
      return req.headers.host;
    });
    app.use(
      logger(":time :method :status :host :realip :url", {
        skip: function (req, res) {
          return res.statusCode == 200 || res.statusCode == 304;
        },
      })
    );
  }
  if (config.proxys && config.proxys.length > 0) {
    console.log(
      "代理1:",
      config.proxys.map((t) => t.path + "=>" + t.target)
    );
    config.proxys.forEach((proxy) => {
      if (!proxy.enable) return;
      app.use(proxy.path, function (req, res, next) {
        console.log(req.method, req.originalUrl);
        // console.log('+++++++++++++++++++++++++++++++',req.method, req.originalUrl);
        if (proxy.showPostData) {
          if (Object.keys(req.query).length > 0) {
            console.log("req.query", req.query);
          }
          if (Object.keys(req.body).length > 0) {
            console.log("req.body", req.body);
          }
        }
        let proxyConfig = {
          target: proxy.target,
          changeOrigin: true,
          onProxyReq: fixRequestBody,
        };
        console.log("proxy.path", proxy.path);
        let pathRewrite=makeRewriteRule(proxy);
        if(pathRewrite){
          proxyConfig.pathRewrite=pathRewrite;
        }
        createProxyMiddleware(proxyConfig)(req, res, next);
      });
    });
  }
  if (config.history) {
    console.log("history模式:", true);
    app.use(history());
  }
  if (config.isAdmin) {
    const router = express.Router();
    router.post("/api/login", login);
    router.post("/api/testStatus", testStatus);
    router.post("/api/manage", manage);
    app.use(router);
  }
  // app.use(express.static(path.join(__dirname, 'public')));
  if (config.staticDirs) {
    console.log("静态目录：", config.staticDirs);
    config.staticDirs.forEach((d) => {
      if (config.isAdmin && d.includes("admin-ui")) {
        let d2 = path.join(__dirname, d);
        if (!fs.existsSync(path.join(d2, "index.html"))) {
          d = path.join(__dirname, "../", d);
        } else {
          d = d2;
        }
        console.log("管理后台网页文件目录：", d);
        //判断文件是否存在
        let indexFile = path.join(d, "index.html");
        if (fs.existsSync(indexFile)) {
          console.log("管理后台网页文件存在：", indexFile);
        } else {
          console.error("管理后台网页文件不存在：", indexFile);
        }
        console.log("管理后台API：http://本地IP:3009/");
      } else if (!d.includes(":") && d.indexOf("/") != 0) {
        d = path.join(__dirname, d);
      }
      console.log("转换后的静态目录：", d);
      app.use(express.static(d));
    });
  }
  return app;
}
//相同端口时
function createProxyApp(config) {
  // console.log("in createProxyApp", config);
  const app = express();
  app.use(compression()); //gzip
  app.use(function (req, res, next) {
    let host = getHost(req);
    let site = config.sites.find((t) => t.host == host);
    if (!site) {
      console.log("unknow proxy host ", host);
      return res.send("unknow proxy host " + host);
    }
    req.site = site;
    if (!site.cors) {
      return next();
    }
    let fun = cors();
    fun(req, res, next);
  });
  app.use(function (req, res, next) {
    let site = req.site;
    if (!site.staticDirs || site.staticDirs.length == 0) {
      return next();
    }
    let fun = express.static(site.staticDirs[0]);
    fun(req, res, next);
  });
  app.use(function (req, res, next) {
    let site = req.site;
    if (!site.proxys || site.proxys.length == 0) {
      return next();
    }
    console.log("req.path", req.path);
    let proxy = site.proxys.find(
      (p) => p.enable && req.path.indexOf(p.path.replace("/*", "")) == 0
    );
    if (!proxy) {
      return next();
    }
    let proxyConfig = {
      target: proxy.target,
      changeOrigin: true,
      onProxyReq: fixRequestBody,
    };
    console.log("proxy.path", proxy.path);
    let pathRewrite=makeRewriteRule(proxy);
    if(pathRewrite){
      proxyConfig.pathRewrite=pathRewrite;
    }
    createProxyMiddleware(proxyConfig)(req, res, next);
  });
  app.use(function (req, res, next) {
    let site = req.site;
    if (!site.history) {
      return next();
    }
    let fun = history();
    fun(req, res, next);
  });
  return app;
}

function makeRewriteRule(proxy) {
  if (proxy.rewrite) {
    console.log("***********包含路径Rewrite**************");
    console.log(proxy.rewrite);
    let arr = proxy.rewrite.split("\n");
    let rewriteConfig = {};
    let needRewrite = false;
    arr.forEach((a) => {
      let tmp = a.split(":");
      if (tmp.length != 2 || !tmp[0] || !tmp[1]) {
        return;
      }
      rewriteConfig[tmp[0]] = tmp[1];
      needRewrite = true;
    });
    if (needRewrite) {
      return rewriteConfig;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

module.exports = {
  createExpressApp,
  createProxyApp,
};

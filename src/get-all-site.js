const { configFormat, clone } = require("./utils");
const db = require("./utils/db");
const adminConfig = require("./controls/admin-web-config");
adminConfig.setDefaultConfig();


function main() {
  const configs = db.get("websites") || [];
  const siteArr = clone(configs);
  let { httpSite, httpsSite, httpProxy, httpsProxy } = configFormat(siteArr);

  if (httpSite.length) {
    console.log(
      "http站点配置数：",
      httpSite.length,
      "端口：",
      httpSite.map((t) => t.http.port)
    );
  }
  if (httpsSite.length) {
    console.log(
      "https站点配置数：",
      httpsSite.length,
      "端口：",
      httpsSite.map((t) => t.https.port)
    );
  }
  if (httpProxy.length) {
    console.log(
      "http代理配置数：",
      httpProxy.length,
      "端口：",
      httpProxy.map((t) => t.port)
    );
  }
  if (httpsProxy.length) {
    console.log(
      "https代理配置数：",
      httpsProxy.length,
      "端口：",
      httpsProxy.map((t) => t.port)
    );
  }
  if (
    httpSite.length == 0 &&
    httpsSite.length == 0 &&
    httpProxy.length == 0 &&
    httpsProxy.length == 0
  ) {
    return console.log("没有启用的站点");
  }
  let allSite=[];
  if (httpSite.length > 0) {
    allSite.push(...httpSite);
  }
  if (httpsSite.length > 0) {
    allSite.push(...httpsSite);
  }
  if (httpProxy.length > 0) {
    allSite.push(...httpProxy);
  }
  if (httpsProxy.length > 0) {
    allSite.push(...httpsProxy);
  }
  return allSite;
}

module.exports = main;

/*
 * @Description:
 * @Autor: fage
 * @Date: 2022-06-06 13:52:09
 * @LastEditors: lanmeng656 lanmeng656@google.com
 * @LastEditTime: 2022-12-20 15:01:02
 */
var net = require("net");
const fs = require("fs");
function timestr(fmt = "MM-dd hh:mm:ss", now = new Date()) {
  var o = {
    "Y+": now.getFullYear(), //月份
    "M+": now.getMonth() + 1, //月份
    "d+": now.getDate(), //日
    "h+": now.getHours(), //小时
    "m+": now.getMinutes(), //分
    "s+": now.getSeconds(), //秒
    "q+": Math.floor((now.getMonth() + 3) / 3), //季度
    S: now.getMilliseconds(), //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(
      RegExp.$1,
      (now.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
      );
  return fmt;
}
function log(...msg) {
  msg.unshift("\n\n【" + timestr() + "】\n");
  console.log(...msg);
}
function obj2arr(obj) {
  let arr = [];
  if (!Array.isArray(obj)) {
    for (key in obj) {
      arr.push(obj[key]);
    }
  } else {
    arr = obj;
  }
  return arr;
}
function getDir(dir) {
  const files = fs.readdirSync(dir);
  const dirs = [];
  for (let f in files) {
    const ff = fs.statSync(dir + files[f]);
    if (ff.isDirectory()) {
      dirs.push(dir + files[f]);
    }
  }
  return dirs;
}
function portIsUsing(port) {
  return new Promise((resolve, reject) => {
    // 创建服务并监听该端口
    var server = net.createServer().listen(port);
    server.on("listening", function () {
      // 执行这块代码说明端口未被占用
      server.close(); // 关闭服务
      // console.log("The port【" + port + "】 is available."); // 控制台输出信息
      resolve(false);
    });
    server.on("error", function (err) {
      if (err.code === "EADDRINUSE") {
        // 端口已经被使用
        // console.log(
        //     "The port【" + port + "】 is occupied, please change other port."
        // );
        resolve(true);
      }
    });
  });
}
module.exports = { timestr, log, obj2arr, getDir, portIsUsing };

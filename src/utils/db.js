const JSONdb = require("simple-json-db");
const path = require("path");
const os = require("os");
const platform = os.platform();
let rootDir = platform == "linux" ? "/etc" : process.cwd();
let dbfile = path.join(rootDir, "/nnws-config.json");
console.log("数据库文件路径：", dbfile);
const db = new JSONdb(dbfile);
const admin = db.get("admin");
if (!admin || !admin.username) {
  db.set("admin", { username: "admin", password: "admin888" });
  console.log("初始化管理员账号密码成功,登录后请及时修改!");
  console.log("管理员初始账号：admin");
  console.log("管理员初始密码：admin888");
}

module.exports = db;

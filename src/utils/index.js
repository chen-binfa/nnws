/*
 * @Description:
 * @Autor: fage
 * @Date: 2022-09-22 17:00:27
 * @LastEditors: lanmeng656 lanmeng656@google.com
 * @LastEditTime: 2023-02-17 14:04:22
 */
const os = require("os");
const tls = require("tls");
const fs = require("fs");
const path = require("path");

module.exports = {
  getIPAdress,
  getHost,
  configFormat,
  getKeyCert, ////多个二级域名SSL验证
  clone,
};
function clone(o) {
  return JSON.parse(JSON.stringify(o));
}
function getHost(req) {
  let host = req.headers.host;
  if (host.includes(":")) {
    host = host.split(":")[0];
  }
  return host;
}
///获取本机ip///
function getIPAdress() {
  var interfaces = os.networkInterfaces();
  for (var devName in interfaces) {
    var iface = interfaces[devName];
    for (var i = 0; i < iface.length; i++) {
      var alias = iface[i];
      if (
        alias.family === "IPv4" &&
        alias.address !== "127.0.0.1" &&
        !alias.internal
      ) {
        return alias.address;
      }
    }
  }
}

//配置文件格式化
function configFormat(config) {
  let http = [];
  let https = [];
  let httpSite = [];
  let httpsSite = [];
  let httpProxy = [];
  let httpsProxy = [];
  config
    .filter((c) => c.enable)
    .forEach((t) => {
      if (t.http && t.http.enable && t.http.port) {
        let site = http.find((h) => h.port == t.http.port);
        if (site) {
          site.arr.push(t);
        } else {
          http.push({
            port: t.http.port,
            arr: [t],
          });
        }
      }
      if (t.https && t.https.enable && t.https.port) {
        let site = https.find((h) => h.port == t.https.port);
        if (site) {
          site.arr.push(t);
        } else {
          https.push({
            port: t.https.port,
            arr: [t],
          });
        }
      }
    });
  http.forEach((h) => {
    if (h.arr.length > 1) {
      let sites = h.arr.map((t) => {
        return {
          id: t.id,
          host: t.host,
          history: t.history,
          cors: t.cors,
          staticDirs: t.staticDirs,
          proxys: t.proxys,
        };
      });
      httpProxy.push({
        port: h.port,
        sites: sites,
        type: "http-proxy",
      });
    } else {
      let o = h.arr[0];
      o.type = "http-site";
      httpSite.push(o);
    }
  });
  https.forEach((h) => {
    if (h.arr.length > 1) {
      let sites = h.arr.map((t) => {
        return {
          id: t.id,
          host: t.host,
          history: t.history,
          cors: t.cors,
          staticDirs: t.staticDirs,
          proxys: t.proxys,
        };
      });
      httpsProxy.push({
        port: h.port,
        sites: sites,
        type: "https-proxy",
      });
    } else {
      let o = h.arr[0];
      o.type = "https-site";
      httpsSite.push(o);
    }
  });
  return {
    httpSite,
    httpsSite,
    httpProxy,
    httpsProxy,
  };
}

//多个二级域名SSL验证
function getKeyCert(httpsWebsites) {
  let KeyCerts = {};
  let SSLKey_default = "";
  let SSLCert_default = "";
  let defaultDomain = "";
  httpsWebsites.forEach((t) => {
    if (!t.host) {
      return console.error("website host is empty.");
    }
    const sslPath = path.join(__dirname, "../ssl/", t.host, "/");
    let keyFile = sslPath + t.host + ".key";
    let certFile = sslPath + t.host + ".crt";
    if (!fs.existsSync(keyFile) || !fs.existsSync(certFile)) {
      return console.error("ssl key or crt file not found",certFile);
    }
    KeyCerts[t.host] = tls.createSecureContext({
      key: fs.readFileSync(keyFile, "utf8"),
      cert: fs.readFileSync(certFile, "utf8"),
      rejectUnauthorized: false,
    });
    if (!SSLKey_default) {
      defaultDomain = t.host;
      SSLKey_default = fs.readFileSync(keyFile, "utf8");
      SSLCert_default = fs.readFileSync(certFile, "utf8");
    }
  });
  let KeyCert = {
    SNICallback: function (domain, cb) {
      if (KeyCerts[domain]) {
        if (cb) {
          cb(null, KeyCerts[domain]);
        } else {
          return KeyCerts[domain]; //兼容Node老版本
        }
      } else {
        //一定要设置默认值，这样访问xyz.comecode.net时，也可以访问，只是会有安全性提示
        if (cb) {
          cb(null, KeyCerts[defaultDomain]);
        } else {
          return KeyCerts[defaultDomain];
        }
      }
    },
    key: SSLKey_default,
    cert: SSLCert_default,
    rejectUnauthorized: false,
  };
  if (httpsWebsites.length == 1) {
    KeyCert = {
      key: SSLKey_default,
      cert: SSLCert_default,
    };
    if (!KeyCert.key) {
      KeyCert = null;
    }
  }
  return KeyCert;
}

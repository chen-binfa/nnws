const jwt = require("jsonwebtoken");
const expiresIn = "5d";
const secret = "E*qw6+3#fds5f";

module.exports = {
  sign: function (data) {
    return jwt.sign(
      {
        data,
      },
      secret,
      { expiresIn }
    );
  },
  verify: function (token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, function (err, decoded) {
        if (err) {
          return resolve(null);
        }
        resolve(decoded);
      });
    });
  },
};

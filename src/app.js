const db = require("./utils/db");
const adminConfig = require("./controls/admin-web-config");
adminConfig.setDefaultConfig();

const { createExpressApp, createProxyApp } = require("./create-app");
const { startHttpServer, startHttpsServer } = require("./start-server");
const getAllSite = require("./get-all-site");

async function main() {
  const configs = db.get("websites") || [];
  let sites = getAllSite();
  for (let config of sites) {
    if (config.type == "http-site") {
      console.log(
        "=============正在启动http站点：",
        config.host,
        "=============="
      );
      config.app = createExpressApp(config);
      const isOn = await startHttpServer(config);
      let web = configs.find((t) => t.id == config.id);
      if (web) {
        web.httpStatus = isOn;
      }
    }
    if (config.type == "https-site") {
      console.log(
        "=============正在启动https站点：",
        config.host,
        "=============="
      );
      config.app = createExpressApp(config);
      const isOn = await startHttpsServer(config);
      let web = configs.find((t) => t.id == config.id);
      if (web) {
        web.httpSStatus = isOn;
      }
    }
    if (config.type == "http-proxy") {
      console.log(
        "=============正在启动http代理：",
        config.port,
        "=============="
      );

      config.app = createProxyApp(config);
      const isOn = await startHttpServer(config);
      config.sites.forEach((t) => {
        let web = configs.find((c) => c.id == t.id);
        if (web) {
          web.httpStatus = isOn;
        } else {
          console.log("*******网站未找到****", t);
        }
      });
    }
    if (config.type == "https-proxy") {
      console.log(
        "=============正在启动https代理：",
        config.port,
        "=============="
      );
      config.app = createProxyApp(config);
      const isOn = await startHttpsServer(config);
      config.sites.forEach((t) => {
        console.log("代理：", t.host, "=>", t.target);
        let web = configs.find((c) => c.id == t.id);
        if (web) {
          web.httpsStatus = isOn;
        } else {
          console.log("*******网站未找到****", t);
        }
      });
    }
  }
  db.set("websites", configs);
  db.sync();
  console.log("启动完毕!");
}

module.exports = main;
